var canvas;
var fft;

var frequencyNb = 512;
var frequencyStartRatio = 0;
var frenquencyEndRatio = 0.5;
var frequencyStart = Math.floor(frequencyNb * frequencyStartRatio);
var frequencyEnd = Math.floor(frequencyNb * frenquencyEndRatio);
var frequencyNbRange = [frequencyStart, frequencyEnd];
var frequencyRangeNbValues = frequencyEnd - frequencyStart;

console.log(frequencyStart)
console.log(frequencyEnd)
var equalizerSize;
var equalizer = new Array(frequencyRangeNbValues);
var equalizerWidth;

var colorMode = "position";
var setWidth = 10;

var isLoading = false;

function preload() {
  isLoading = true;
  sound = loadSound('public/sound/rock_robot.mp3', () => {isLoading = false});
}

function setup() {
  fft = new p5.FFT(0.8, frequencyNb);
  canvas = createCanvas(720, 720, window.height);
  //Listeners
  canvas.mouseClicked(togglePlay);


  //Main


  sound.amp(0.2);
  if (setWidth > 0) {
    equalizerWidth = setWidth;
  } else {
    equalizerWidth = canvas.width / equalizer.length;
  }
  for (i = 0; i < frequencyRangeNbValues; i++) {
    let frequenceNumber = frequencyStart + i;
    equalizer[i] = new EqualizerBar({ x: 0, y: 0 }, equalizerWidth, 20, frequenceNumber);
  }
}

var spectrum;
function draw() {
  background(0);
  if (isLoading) {
    fill(255);
    translate(width / 2-50, height / 2);
    textSize(32);
    text('Loading...', 0,0);
  }else{
    spectrum = fft.analyze();
    noStroke();
    translate(width / 2, height / 2);

    for (i = 0; i < equalizer.length; i++) {
      frequencyValue = spectrum[equalizer[i].frequenceNumber];
      var hsl = Math.floor(i / equalizer.length * 360);
      switch (colorMode) {
        case "position":
          hsl = Math.floor(i / equalizer.length * 360);
          break;
        case "size":
          hsl = Math.floor(frequencyValue / 500 * 360);
          break;
        default:
      }
      equalizer[i].height = frequencyValue;
      fill('hsl(' + hsl + ', 100%, 50%)');
      rect(equalizer[i].position.x - (equalizer[i].width / 9), equalizer[i].position.y + 50, equalizer[i].width / 4, equalizer[i].height + 1);
      rotate(PI * 2 / (equalizer.length));
    }
  }
}

function togglePlay() {
  if (sound.isPlaying()) {
    sound.pause();
  } else {
    sound.loop();
  }
}

function stopSound() {
  sound.stop();
}

function resetSound() {
  sound.stop();
  sound.loop();
}

function switchColorMode(mode) {
  colorMode = mode;
  console.log("Switched to " + mode);
}

function loadLocalSong(button) {
  var music_values = document.getElementById('music-values');
  var music = music_values[music_values.selectedIndex].value;
  loadSong('public/sound/' + music, button);
}

function loadURLSong(button) {
  var music_url = document.getElementById('music-url');
  var music_value = music_url.value;
  loadSong(music_value, button);
}

function loadSong(songUrl, button) {
  isLoading = true;
  if (sound)
    sound.stop();
  if (button)
    button.innerHTML = "Loading...";
  sound.setPath(songUrl,
    //Success
    () => {
      console.log("Loading done");
      if (button) {
        button.innerHTML = "Loading done.";
        setTimeout(function () { button.innerHTML = "Load"; }, 2000);
      }
      if (!sound.isPlaying())
        sound.play();
        
      isLoading = false;
    },
    //Error
    () => {
      console.log("Loading failed.");
      if (button) {
        button.innerHTML = "Loading failed.";
        setTimeout(function () { button.innerHTML = "Load"; }, 2000);
      }
      isLoading = false;
    }
  );
}
