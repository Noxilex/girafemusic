# Changelog

## 13/12/2018
- Added the possibility in the code to show only a certain range of frenquencies (to prevent empty frequencies taking too much space)
- Fixed the problem where loading an empty URL would cause the Loading.. button to not go back to Load (Exception was thrown without calling the callback error method)
- Fixed the display of rectangles, their position was a bit crooked. 

## 12/12/2018
- Working version with equalizer-like (on one line) display
- Changed from one line to circle display
- Added menu to modify values on the go
- Menu:
  - Options to change the color based on position or size
  - Options to control the flow of the music (play/pause/stop/reset)
  - Option to change the music based on a fixed list (currently implemented in the HTML != dynamic)
  - Option to change the music based on a URL.

### TODO:
- Implement volume option
- Implement number of frenquency option
- Implement width of frequency option
- Implement height ratio of frequency option
- Make the list of music dynamic based on an array in the code ?
